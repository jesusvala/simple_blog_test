# Blog

## Introduction

This is a simple blog created as a test for a job application

## Installation

This app requires [Node.js](https://nodejs.org/) v12+ to run.

First, you need to duplicate the file .env.example with the name .env and fill with your own variables, not forgetting database information.

Then just run the following commands to install all dependences and run all db migrations

```sh
composer install
npm run install
npm run dev
composer dump-autoload
php artisan migrate
```

Finally just run the develop server with:

```sh
php artisan serve
```